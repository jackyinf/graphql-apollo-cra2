import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import {ApolloClient} from "apollo-client";
import {createHttpLink} from "apollo-link-http";
import {InMemoryCache} from "apollo-cache-inmemory";
import {ApolloProvider} from "react-apollo";
import { setContext } from 'apollo-link-context';
import ErrorBoundary from 'react-error-boundary';
import {BrowserRouter as Router, Route} from 'react-router-dom';
import GithubUserInfo from "./GithubUserInfo.component";

export const link = createHttpLink({uri: "http://localhost:4000/graphql"});
export const client = new ApolloClient({cache: new InMemoryCache(), link});

const httpLink = createHttpLink({uri: 'https://api.github.com/graphql'});
const githubLink = setContext((_: any, { headers }: any) => {
  const token = process.env.REACT_APP_GITHUB_TOKEN;
  return {headers: {...headers, authorization: token ? `Bearer ${token}` : ""}}
});
export const githubClient = new ApolloClient({cache: new InMemoryCache(), link: githubLink.concat(httpLink)});

ReactDOM.render(
    <ErrorBoundary>
      <Router>
        <div>
          <ApolloProvider client={client}>
            <Route path="/" exact={true} component={App} />
          </ApolloProvider>
          <ApolloProvider client={githubClient}>
            <Route path="/user-info" exact={true} component={GithubUserInfo} />
          </ApolloProvider>
        </div>
      </Router>
    </ErrorBoundary>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
