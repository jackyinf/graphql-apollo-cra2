import React from 'react';
import gql from 'graphql-tag';
import {Query} from 'react-apollo';

interface Course {
  id: string;
  title: string;
}

const GET_COURSES = gql`
  {
    courses {
      id
      title
    }
  }
`;

const Courses = ({}) => (
  <Query query={GET_COURSES}>
    {({loading, error, data}) => {
      if (loading) return 'Loading...';
      if (error) return `Error! ${error.message}`;

      return (
        <ul>
          {data.courses.map((course: Course) => (
            <li key={course.id}>#{course.id}: {course.title}</li>
          ))}
        </ul>
      );
    }}
  </Query>
);

export default Courses;
