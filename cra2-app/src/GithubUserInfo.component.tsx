import React from 'react';
import gql from 'graphql-tag';
import {Query, QueryResult} from 'react-apollo';

interface UserNode {
  name: string
}

interface UsarEdge {
  node: UserNode
}

interface RepositoriesInfo {
  edges: Array<UsarEdge>,
  totalCount: number
}

interface User {
  name: string;
  repositories: RepositoriesInfo
}

const GET_USER_INFO = gql`
query {
  user(login: "jackinf") {
    name,
    repositories(first: 5, orderBy: {field: CREATED_AT, direction:ASC}) {
      edges {
        node {
          name
        }
      },
      totalCount
    }
  }
}
`;

const UserInfo = ({}) => (
  <Query query={GET_USER_INFO}>
    {({loading, error, data}: QueryResult<{user: User}>) => {
      if (loading) return "Loading...";
      if (error) return `Error... ${error.message}`;
      if (!data) return "No result.";

      return (
        <div>
          User info.
          <ul>
            <li>Name: {data.user.name}</li>
            <li>Number of repositories: {data.user.repositories.totalCount}</li>
          </ul>
        </div>
      )
    }}
  </Query>
);

export default UserInfo;
